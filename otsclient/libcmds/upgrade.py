from typing import BinaryIO

from opentimestamps.calendar import DEFAULT_CALENDAR_WHITELIST

from ..cmds import upgrade_command as upgrade_cmd
from ..cache import TimestampCache


class UpgradeArgs:

    def __init__(self, ots_file: BinaryIO, cache_path: str):
        self.cache = TimestampCache(cache_path)
        self.files = [ots_file]
        self.whitelist = DEFAULT_CALENDAR_WHITELIST
        self.dry_run = False
        self.calendar_urls = []
        self.wait = False


def upgrade_command(ots_file_path, cache_path) -> bool:
    """
    :param ots_file_path: the path of the proof to upgrade
    :param cache_path: path of the ots cache
    :return True if the stamp is complete
    """
    with open(ots_file_path, 'rb') as ots_fd:
        args = UpgradeArgs(ots_fd, cache_path)

        return upgrade_cmd(args)
