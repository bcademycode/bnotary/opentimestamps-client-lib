import binascii
from typing import BinaryIO

from opentimestamps.calendar import DEFAULT_CALENDAR_WHITELIST

from ..cmds import verify_command as verify_cmd
from ..cache import TimestampCache
from ..esplora import EsploraProxy


def setup_bitcoin():

    # try every explorer twice
    return EsploraProxy([
        'https://blockstream.info/api',
        'https://esplora.inbitcoin.it/api',
        'https://esplora.sanuscoin.com/api',
    ])


class VerifyArgs:

    def __init__(self, ots_file: BinaryIO, file_digest: bytes, cache_path: str):
        self.cache = TimestampCache(cache_path)
        self.timestamp_fd = ots_file
        self.whitelist = DEFAULT_CALENDAR_WHITELIST
        self.wait = False
        self.hex_digest = binascii.hexlify(file_digest).decode('utf-8')
        self.use_bitcoin = True
        self.setup_bitcoin = setup_bitcoin


def verify_command(file_digest: bytes, ots_file_path: str, cache_path: str) -> int:
    """
    :param file_digest: a sha256 hash, in bytes
    :param ots_file_path: the path of the proof to verify
    :param cache_path: path of the ots cache
    :return block height if the stamp is complete, 0 otherwise
    """
    with open(ots_file_path, 'rb') as ots_fd:
        args = VerifyArgs(ots_fd, file_digest, cache_path)
        blocks_height = verify_cmd(args)
        return min(blocks_height) if blocks_height else 0
