from typing import BinaryIO, List

from ..cmds import info_command as info_cmd


class InfoArgs:

    def __init__(self, ots_file: BinaryIO):
        self.file = ots_file
        self.verbosity = 0


def get_salt(timestamp_tree: List[str]) -> str:
    row = timestamp_tree[0]
    if not row.startswith('append '):
        raise ValueError('Salt not found in row %s' % (row, ))
    salt = row[7:]
    return salt


def info_command(ots_file_path: str) -> dict:
    """
    :param ots_file_path: the path of the proof to verify
    :return structured printable data
    """
    with open(ots_file_path, 'rb') as ots_fd:
        args = InfoArgs(ots_fd)
        file_digest, timestamp_tree = info_cmd(args)
        timestamp_tree = timestamp_tree.split('\n')
        salt = get_salt(timestamp_tree)
        return dict(file_digest=file_digest, salt=salt)
