import io
import binascii
from typing import BinaryIO

from opentimestamps.calendar import DEFAULT_CALENDAR_WHITELIST

from ..cmds import stamp_command as stamp_cmd
from ..cmds import upgrade_command as upgrade_cmd
from ..cmds import verify_command as verify_cmd
from ..cache import TimestampCache
from ..esplora import EsploraProxy

class StampArgs:

    def __init__(self, file_digest, ots_file_path):
        self.file_digest = file_digest
        self.ots_file_path = ots_file_path

        # files cannot be empty, we use a virtual void file
        self.files = [io.BytesIO(b'')]

        self.calendar_urls = []
        self.wait = False
        self.use_btc_wallet = False
        self.timeout = 5
        self.m = 2


def stamp_command(file_digest: bytes, ots_file_path: str) -> bool:
    """
    :param file_digest: a sha256 of notarizing data
    :param ots_file_path: path of the output ots file
    :return True if at least m calendars accept the timestamp
    """
    args = StampArgs(file_digest, ots_file_path)

    return stamp_cmd(args)
