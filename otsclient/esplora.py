# Copyright (C) 2023 Bcademy srl
#
# This file is part of the OpenTimestamps Client.
#
# It is subject to the license terms in the LICENSE file found in the top-level
# directory of this distribution.
#
# No part of the OpenTimestamps Client, including this file, may be copied,
# modified, propagated, or distributed except according to the terms contained
# in the LICENSE file.
import logging
from random import shuffle
from typing import List, Any, Callable

import requests
from bitcoin.core import lx, b2lx, x, CBlockHeader


class ProxyError(Exception):
    pass


class EsploraProxy:
    """
    Interface with the Esplora blockchain explorer
    """
    def __init__(self, service_urls: List[str]):
        """
        :param service_urls API deployment URLs (e.g. https://blockstream.info/api)
        """
        assert all(service_url[-1] != '/' for service_url in service_urls)
        self.urls = service_urls

    def _shuffled_urls(self):
        """Return a list of shuffled urls, every explorer appears twice."""
        urls = self.urls[:]
        shuffle(urls)
        return urls * 2

    def _retry(self, function: Callable[[str], Any]) -> Any:
        """
        :param function takes an url as parameter
        :return the result of function at the first execution without errors, otherwise raise the last exception
        """
        # try every explorer twice
        exception = None

        for url in self._shuffled_urls():
            try:
                res = function(url)
            except Exception as ex:
                logging.error('Esplora connection: %s' % (url, ))
                exception = ProxyError(ex)
            else:
                return res
        raise exception

    def getblockcount(self) -> int:

        def run(url: str):
            return int(requests.get(url + '/blocks/tip/height').content.decode('ascii'))

        return self._retry(run)

    def getblockhash(self, height) -> bytes:

        def run(url: str):
            return lx(requests.get(url + '/block-height/%d' % height).content.decode('ascii'))

        return self._retry(run)

    def getblockheader(self, block_hash: bytes) -> CBlockHeader:

        def run(url: str):
            raw_block_header = x(requests.get(url + '/block/%s/header' % b2lx(block_hash)).content.decode('ascii'))
            return CBlockHeader.deserialize(raw_block_header)

        return self._retry(run)
