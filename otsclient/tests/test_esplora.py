# Copyright (C) 2018 The OpenTimestamps developers
# Copyright (C) 2021 Bcademy srl
#
# This file is part of the OpenTimestamps Client.
#
# It is subject to the license terms in the LICENSE file found in the top-level
# directory of this distribution.
#
# No part of the OpenTimestamps Client, including this file, may be copied,
# modified, propagated, or distributed except according to the terms contained
# in the LICENSE file.

import unittest
from unittest.mock import patch
from bitcoin.core import lx, CBlockHeader

from otsclient.esplora import EsploraProxy


class TestEsplora(unittest.TestCase):

    def setUp(self):
        self.proxy = EsploraProxy(['https://mock/api'])

    @patch('requests.get')
    def test_getblockcount(self, mock_get):
        """Get block count"""
        mock_get().content = b'2002'

        self.assertEqual(self.proxy.getblockcount(), 2002)

    @patch('requests.get')
    def test_getblockhash(self, mock_get):
        """Get block hash"""
        mock_get().content = b'00000000504d5fa0ad2cb90af16052a4eb2aea70fa1cba653b90a4583c5193e4'

        self.assertEqual(self.proxy.getblockhash(40000),
                         lx('00000000504d5fa0ad2cb90af16052a4eb2aea70fa1cba653b90a4583c5193e4'))

    @patch('requests.get')
    def test_getblockheader(self, mock_get):
        """Get the block header"""
        mock_get().content = b'010000004599c0b2f9e410d3d45d48bbfcb1456a3033fe891bd2a113a0f3092500000000cd5' \
                             b'e83daaf5134f252a37002ec6d6ccfbd88611866573c936037add788f837534895764bc38c001d1c8012cf'
        block_header = CBlockHeader(1, lx('000000002509f3a013a1d21b89fe33306a45b1fcbb485dd4d310e4f9b2c09945'), lx('5337f888d7ad3760933c5766186188bdcf6c6dec0270a352f23451afda835ecd'), 1266062664, 0x1d008cc3, 0xcf12801c)

        self.assertEqual(self.proxy.getblockheader(
            lx('00000000504d5fa0ad2cb90af16052a4eb2aea70fa1cba653b90a4583c5193e4')), block_header)
