#!/usr/bin/env python3

import os
import shutil

from bitcoin.core import x

from otsclient.libcmds import *

JUST_RES = 10
CACHE_PATH = '/tmp/cache-test'


def stamp():
    ots_path = '/tmp/out.ots'
    try:
        os.remove(ots_path)
    except FileNotFoundError:
        pass
    res = stamp_command(b'1'.ljust(32, b'0'), ots_path)
    print('[stamp]'.ljust(JUST_RES), res)


def upgrade():
    ots_path = 'incomplete.txt.ots'
    ots_path_bak = ots_path + '.bak'
    shutil.copyfile('examples/incomplete.txt.ots', ots_path)
    if os.path.isfile(ots_path_bak):
        os.remove(ots_path_bak)

    res = upgrade_command(ots_path, CACHE_PATH)

    os.remove(ots_path)
    os.remove(ots_path_bak)
    print('[upgrade]'.ljust(JUST_RES), res)


def verify():
    ots_path = 'two-calendars.txt.ots'
    shutil.copyfile('examples/two-calendars.txt.ots', ots_path)
    file_digest = x('efaa174f68e59705757460f4f7d204bd2b535cfd194d9d945418732129404ddb')
    res = verify_command(file_digest, ots_path, CACHE_PATH)
    os.remove(ots_path)
    print('[verify]'.ljust(JUST_RES), res, ots_path)


def info():
    ots_path = 'two-calendars.txt.ots'
    shutil.copyfile('examples/two-calendars.txt.ots', ots_path)
    res = info_command(ots_path)
    os.remove(ots_path)
    print('[info]'.ljust(JUST_RES), res, ots_path)


def main():
    stamp()
    upgrade()
    verify()
    info()


if __name__ == '__main__':
    main()
